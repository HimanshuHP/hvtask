package com.himanshuph.hourvtask

import android.content.Context
import com.himanshuph.hourvtask.data.source.UserActivityRespositry
import com.himanshuph.hourvtask.data.source.remote.RestProvider
import com.himanshuph.hourvtask.data.source.remote.UserActivityRemoteDataSource

object Injection {

    fun provideUserActivityRepository(): UserActivityRespositry {
        return UserActivityRespositry.getInstance(UserActivityRemoteDataSource.getInstance(RestProvider.getUserActivityService()))
    }
}