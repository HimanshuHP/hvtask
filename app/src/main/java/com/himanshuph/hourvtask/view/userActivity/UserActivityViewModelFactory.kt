package com.himanshuph.hourvtask.view.userActivity

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.himanshuph.hourvtask.data.source.UserActivityDataSource
import com.himanshuph.hourvtask.utils.rx.SchedulerProvider


class UserActivityViewModelFactory(val userActivityDataSource: UserActivityDataSource, val schedulerProvider: SchedulerProvider) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserActivityViewModel::class.java)) {
            return UserActivityViewModel(userActivityDataSource, schedulerProvider) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}