package com.himanshuph.hourvtask.view.userActivity

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import android.view.View
import com.himanshuph.hourvtask.R
import com.himanshuph.hourvtask.data.model.ActivitiesItem
import com.himanshuph.hourvtask.data.model.Result
import com.himanshuph.hourvtask.data.source.UserActivityDataSource
import com.himanshuph.hourvtask.utils.rx.SchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class UserActivityViewModel(val userActivityDataSource: UserActivityDataSource, val schedulerProvider: SchedulerProvider) : ViewModel() {
    private val mCompositeDisposable = CompositeDisposable()

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMsg: MutableLiveData<Int> = MutableLiveData()
    val giveActivityListData: MutableLiveData<ArrayList<ActivitiesItem>> = MutableLiveData()
    val getActivityListData: MutableLiveData<ArrayList<ActivitiesItem>> = MutableLiveData()

    fun fetchActivityList(userId: String, role: String, query: String = ""): Observable<Result<ArrayList<ActivitiesItem>>> {
        return userActivityDataSource.getUserActivities(userId, role, query)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { onRetrieveActivityListStart() }
                .doOnTerminate { onRetrieveActivityListFinish() }
    }


    fun fetchGiveActivityList(userId: String, query: String = "") {
        mCompositeDisposable.add(fetchActivityList(userId, "give", query)
                .subscribe(
                        { result: Result<ArrayList<ActivitiesItem>> ->
                            when {
                                result.isSuccess() -> {
                                    result.data?.let { onRetrieveGiveActivitySuccess(it) }
                                }
                                result.isError() -> {
                                    onRetrieveActivityError()
                                }
                            }
                        },
                        { onRetrieveActivityError() }
                ))
    }

    fun fetchGetActivityList(userId: String, query: String = "") {

        mCompositeDisposable.add(fetchActivityList(userId, "get", query)
                .subscribe(
                        { result: Result<ArrayList<ActivitiesItem>> ->
                            when {
                                result.isSuccess() -> {
                                    result.data?.let { onRetrieveGetActivitySuccess(it) }
                                }
                                result.isError() -> {
                                    onRetrieveActivityError()
                                }
                            }
                        },
                        { onRetrieveActivityError() }
                ))
    }



    private fun onRetrieveActivityListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMsg.value = null
    }

    private fun onRetrieveActivityListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveGiveActivitySuccess(activityList: ArrayList<ActivitiesItem>) {
        Log.e("Give Success", "yee")
        giveActivityListData.value = activityList
    }

    private fun onRetrieveGetActivitySuccess(activityList: ArrayList<ActivitiesItem>) {
        Log.e("Get Success", "yee")
        getActivityListData.value = activityList
    }

    private fun onRetrieveActivityError() {
        errorMsg.value = R.string.activity_error
    }


    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.dispose()
    }
}