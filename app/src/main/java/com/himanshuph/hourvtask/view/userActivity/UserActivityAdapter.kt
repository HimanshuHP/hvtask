package com.himanshuph.hourvtask.view.userActivity

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.himanshuph.hourvtask.R
import com.himanshuph.hourvtask.data.model.ActivitiesItem
import com.himanshuph.hourvtask.utils.CommonUtils.getFormattedDate
import com.himanshuph.hourvtask.utils.inflate

class UserActivityAdapter(var userActivityList: ArrayList<ActivitiesItem>) : RecyclerView.Adapter<UserActivityAdapter.ActivityItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityItemViewHolder = ActivityItemViewHolder(parent.inflate(R.layout.activity_list_item))

    override fun onBindViewHolder(holder: ActivityItemViewHolder, position: Int) {
        val activitiesItem = userActivityList[holder.adapterPosition]
        showMediaImage(holder.mediaIv, activitiesItem.mediaUrls?.get(0)?: "",holder.view.context)
        showUserImage(holder.userIv, activitiesItem.creator?.profilePic?: "",holder.view.context)
        holder.timeTv.text = activitiesItem.timeDetails?.label?:""
        holder.userNameTv.text = "by ${activitiesItem.creator?.name}"
        holder.titleTv.text = activitiesItem.title?:""
        holder.sessionCountTv.text = "${activitiesItem.sessionDetails?.sessionIndividualCount?:0} session | One-on-One"
        holder.locationTv.text = activitiesItem.sessionDetails?.session?.locationDetails?.locationName?:""
        holder.dateTv.text = getFormattedDate(activitiesItem.sessionDetails?.session?.date?:"")
        holder.favoriteIv.setImageDrawable(ContextCompat.getDrawable(holder.view.context, if (activitiesItem.isFavorited) R.drawable.like else R.drawable.unlike))
    }

    fun setData(userActivityList: ArrayList<ActivitiesItem>) {
        this.userActivityList = userActivityList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = userActivityList.size

    fun showMediaImage(mediaIv: ImageView, imageUrl: String, context: Context) {
        try {
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.singapore)
                    .error(R.drawable.singapore)
                    .into(mediaIv)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showUserImage(userIv: ImageView, imageUrl: String, context: Context) {
        try {
            Glide.with(context)
                    .load(imageUrl)
                    .placeholder(R.drawable.user_avatar_circular)
                    .error(R.drawable.user_avatar_circular)
                    .into(userIv)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    class ActivityItemViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val mediaIv = view.findViewById<ImageView>(R.id.mediaIv)
        val favoriteIv = view.findViewById<ImageView>(R.id.favoriteIv)
        val userIv = view.findViewById<ImageView>(R.id.userIv)
        val timeTv = view.findViewById<TextView>(R.id.timeTv)
        val userNameTv = view.findViewById<TextView>(R.id.userNameTv)
        val sessionCountTv = view.findViewById<TextView>(R.id.sessionCountTv)
        val titleTv = view.findViewById<TextView>(R.id.titleTv)
        val dateTv = view.findViewById<TextView>(R.id.dateTv)
        val locationTv = view.findViewById<TextView>(R.id.locationTv)
    }
}