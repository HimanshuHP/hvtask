package com.himanshuph.hourvtask.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.himanshuph.hourvtask.R
import com.himanshuph.hourvtask.view.userActivity.UserActivityListFragment
import com.roughike.bottombar.OnTabSelectListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnTabSelectListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigation.setDefaultTab(R.id.explore_view)
        bottomNavigation.setOnTabSelectListener(this)
    }

    override fun onTabSelected(tabId: Int) {
        onFragmentChanged(supportFragmentManager, tabId)
    }

    fun onFragmentChanged(fragmentManager: FragmentManager, tabId: Int) {
        try {
            val currentVisible = getVisibleFragment(fragmentManager)
            val userActivityListFragment = getFragmentByTag(fragmentManager, UserActivityListFragment.TAG) as UserActivityListFragment?
            when (tabId) {
                R.id.explore_view -> {
                    if (userActivityListFragment == null)
                        onAddAndHide(fragmentManager, UserActivityListFragment.newInstance(), currentVisible)
                    else
                        onShowHideFragment(fragmentManager, userActivityListFragment, currentVisible)

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun onShowHideFragment(fragmentManager: FragmentManager, toShow: Fragment?, toHide: Fragment?) {
        if (toHide == null) {
            fragmentManager
                    .beginTransaction()
                    .show(toShow)
                    .commit()
            toShow?.onHiddenChanged(false)
        } else {
            toHide.onHiddenChanged(true)
            fragmentManager
                    .beginTransaction()
                    .hide(toHide)
                    .show(toShow)
                    .commit()
            toShow?.onHiddenChanged(false)
        }
    }

    fun onAddAndHide(fragmentManager: FragmentManager, toAdd: Fragment, toHide: Fragment?) {

        if (toHide == null) {
            fragmentManager
                    .beginTransaction()
                    .add(R.id.container, toAdd, toAdd.javaClass.simpleName)
                    .commit()
            toAdd.onHiddenChanged(false)
        } else {
            toHide.onHiddenChanged(true)
            fragmentManager
                    .beginTransaction()
                    .hide(toHide)
                    .add(R.id.container, toAdd, toAdd.javaClass.simpleName)
                    .commit()
            toAdd.onHiddenChanged(false)
        }
    }

    companion object {

        fun getFragmentByTag(fragmentManager: FragmentManager, tag: String): Fragment? {
            return fragmentManager.findFragmentByTag(tag)
        }

        fun getVisibleFragment(manager: FragmentManager): Fragment? {
            val fragments = manager.fragments
            if (fragments != null && !fragments.isEmpty()) {
                for (fragment in fragments) {
                    if (fragment != null && fragment.isVisible) {
                        return fragment
                    }
                }
            }
            return null
        }
    }
}
