package com.himanshuph.hourvtask.view.userActivity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import co.ceryle.segmentedbutton.SegmentedButtonGroup
import com.himanshuph.hourvtask.Injection
import com.himanshuph.hourvtask.R
import com.himanshuph.hourvtask.data.model.ActivitiesItem
import com.himanshuph.hourvtask.utils.gone
import com.himanshuph.hourvtask.utils.inflate
import com.himanshuph.hourvtask.utils.rx.AppSchedulerProvider
import com.himanshuph.hourvtask.utils.toast
import com.himanshuph.hourvtask.utils.visible
import kotlinx.android.synthetic.main.fragment_user_activity_list.*


class UserActivityListFragment : Fragment() {

    lateinit var userActivityAdapter: UserActivityAdapter

    private lateinit var viewModel: UserActivityViewModel

    lateinit var viewModelFactory: UserActivityViewModelFactory

    var giveSearchQuery = ""
    var getSearchQuery = ""

    var position: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelFactory = UserActivityViewModelFactory(Injection.provideUserActivityRepository(), AppSchedulerProvider())
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserActivityViewModel::class.java)
        userActivityAdapter = UserActivityAdapter(arrayListOf())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_user_activity_list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        viewModel.errorMsg.observe(this, errorMsgObserver)
        viewModel.loadingVisibility.observe(this, loadingVisibilityObserver)
        viewModel.giveActivityListData.observe(this, giveActivityListDataObserver)
        viewModel.getActivityListData.observe(this, getActivityListDataObserver)
        searchEt.setOnEditorActionListener(onSearchActionListener)
        chipView.setOnDeleteClicked(deleteClickListener)
        chipView.gone()
        fetchGiveList()
        segmentBG.setOnPositionChangedListener(onTabPositionChangedListener)
        position = 0
        segmentBG.setPosition(position, 0)
    }

    val deleteClickListener = View.OnClickListener {
        when (position) {
            0 -> {
                giveSearchQuery = ""
                fetchGiveList(giveSearchQuery)
            }
            1 -> {
                getSearchQuery = ""
                fetchGetList(getSearchQuery)
            }
        }
        chipView.label = ""
        chipView.gone()
    }

    val onSearchActionListener = TextView.OnEditorActionListener { textView, actionId, event ->
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            val searchedText = textView.text.toString().trim()
            if (searchedText.isNotEmpty()) {
                when (position) {
                    0 -> {
                        giveSearchQuery = searchedText
                        chipView.label = giveSearchQuery
                        chipView.visible()
                        fetchGiveList(giveSearchQuery)
                    }
                    1 -> {
                        getSearchQuery = searchedText
                        chipView.label = getSearchQuery
                        chipView.visible()
                        fetchGetList(getSearchQuery)
                    }
                }
                searchEt.setText("")
            } else
                context.toast("Search Text cannot be empty")


            return@OnEditorActionListener true
        }
        false
    }


    val onTabPositionChangedListener = SegmentedButtonGroup.OnPositionChangedListener { position ->
        Log.e("position changed", position.toString())
        if (position == this@UserActivityListFragment.position)
            return@OnPositionChangedListener

        this@UserActivityListFragment.position = position

        when (position) {
            0 -> {
                val giveList = viewModel.giveActivityListData.value
                if (giveSearchQuery.isNotEmpty()) {
                    chipView.label = giveSearchQuery
                    chipView.visible()

                    updateRecyclerView(giveList)
                } else if (giveList != null && giveList.isNotEmpty()) {
                    updateRecyclerView(giveList)
                    chipView.gone()
                } else {
                    chipView.gone()
                    fetchGiveList()
                }
            }
            1 -> {
                val getList = viewModel.getActivityListData.value
                if (getSearchQuery.isNotEmpty()) {
                    chipView.label = getSearchQuery
                    chipView.visible()
                    updateRecyclerView(getList)
                } else if (getList != null && getList.isNotEmpty()) {
                    chipView.gone()
                    updateRecyclerView(getList)
                } else {
                    chipView.gone()
                    fetchGetList()
                }
            }
        }
    }

    private fun fetchGiveList(query: String = "") {
        viewModel.fetchGiveActivityList("d87eebc0ac661da3c7b20b76ffc238e5", query)
    }

    private fun fetchGetList(query: String = "") {
        viewModel.fetchGetActivityList("d87eebc0ac661da3c7b20b76ffc238e5", query)
    }

    val giveActivityListDataObserver: Observer<ArrayList<ActivitiesItem>> = Observer { activityList ->
        if (position == 0)
            updateRecyclerView(activityList)
    }

    val getActivityListDataObserver: Observer<ArrayList<ActivitiesItem>> = Observer { activityList ->
        if (position == 1)
            updateRecyclerView(activityList)
    }

    private fun updateRecyclerView(activityList: ArrayList<ActivitiesItem>?) {
        activityList?.let {
            userActivityRv.let {
                it.layoutManager = LinearLayoutManager(context)
                it.adapter = userActivityAdapter
            }
            userActivityAdapter.setData(it)
            userActivityRv.visible()
        } ?: userActivityRv.gone()
    }

    val loadingVisibilityObserver: Observer<Int> = Observer { visibility ->
        visibility?.let { updateLoaderView(it) } ?: updateLoaderView(View.GONE)
    }

    fun updateLoaderView(visibility: Int) {
        loader.visibility = visibility
    }

    val errorMsgObserver: Observer<Int> = Observer { errMsg ->

        if (errMsg != null) showError(errMsg) else hideError()
    }

    fun showError(errorMsg: Int) {
        errorTv.text = getString(errorMsg)
        errorTv.visible()
    }

    fun hideError() {
        errorTv.gone()
    }


    companion object {

        @JvmField
        val TAG = UserActivityListFragment::class.java.simpleName

        @JvmStatic
        fun newInstance() =
                UserActivityListFragment().apply {
                }
    }
}
