package com.himanshuph.hourvtask.data.source.remote

import com.himanshuph.hourvtask.data.model.ApiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface UserActivityService {

    @GET("activity/list/")
    fun getUserActivityResponse(@Query("u") userId: String = "d87eebc0ac661da3c7b20b76ffc238e5",
                                @Query("role") role: String,
                                @Query("q") query: String = "", @Query("test") test: Boolean = true): Observable<ApiResponse>
}