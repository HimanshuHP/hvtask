package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class SkillsItem(

	@SerializedName("name")
	val name: String? = null,

	@SerializedName("id")
	val id: String? = null
)