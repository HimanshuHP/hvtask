package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class Creator(

	@SerializedName("name")
	val name: String? = null,

	@SerializedName("profile_pic")
	val profilePic: String? = null,

	@SerializedName("external_id")
	val externalId: String? = null,

	@SerializedName("id")
	val id: String? = null,

	@SerializedName("username")
	val username: String? = null
)