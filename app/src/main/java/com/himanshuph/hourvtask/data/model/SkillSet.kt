package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class SkillSet(

	@SerializedName("skills")
	val skills: List<SkillsItem>? = null,

	@SerializedName("categories")
	val categories: List<CategoriesItem>? = null
)