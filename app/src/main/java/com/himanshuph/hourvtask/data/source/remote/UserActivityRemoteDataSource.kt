package com.himanshuph.hourvtask.data.source.remote

import com.himanshuph.hourvtask.data.model.ActivitiesItem
import com.himanshuph.hourvtask.data.model.ApiResponse
import com.himanshuph.hourvtask.data.model.Result
import com.himanshuph.hourvtask.data.source.UserActivityDataSource
import io.reactivex.Observable

class UserActivityRemoteDataSource(val userActivityService: UserActivityService) : UserActivityDataSource {

    override fun getUserActivities(userId: String, role: String, query: String): Observable<Result<ArrayList<ActivitiesItem>>> {
        return userActivityService.getUserActivityResponse(userId, role, query)
                .map { apiResponse: ApiResponse ->
                    if (apiResponse.code == 200)
                        Result.fromData(apiResponse.activities?.let { it } ?: ArrayList())
                    else
                        Result.fromError(Exception(apiResponse.errorMsg))
                }
    }

    companion object {

        private lateinit var INSTANCE: UserActivityRemoteDataSource
        private var needsNewInstance = true

        @JvmStatic
        fun getInstance(userActivityService: UserActivityService): UserActivityRemoteDataSource {
            if (needsNewInstance) {
                INSTANCE = UserActivityRemoteDataSource(userActivityService)
                needsNewInstance = false
            }
            return INSTANCE
        }
    }
}