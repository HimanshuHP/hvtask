package com.himanshuph.hourvtask.data.source

import com.himanshuph.hourvtask.data.model.ActivitiesItem
import com.himanshuph.hourvtask.data.model.Result
import io.reactivex.Observable

class UserActivityRespositry(val userActivityRemoteDataSource: UserActivityDataSource) : UserActivityDataSource {

    override fun getUserActivities(userId: String, role: String, query: String): Observable<Result<ArrayList<ActivitiesItem>>> {
        return userActivityRemoteDataSource.getUserActivities(userId, role, query)
    }

    companion object {

        private var INSTANCE: UserActivityRespositry? = null

        @JvmStatic
        fun getInstance(userActivityRemoteDataSource: UserActivityDataSource): UserActivityRespositry {
            return INSTANCE ?: UserActivityRespositry(userActivityRemoteDataSource)
                    .apply { INSTANCE = this }
        }

        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}