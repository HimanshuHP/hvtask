package com.himanshuph.hourvtask.data.model

import com.google.gson.annotations.SerializedName

data class ActivitiesItem(

	@SerializedName("skill_set")
	val skillSet: SkillSet? = null,

	@SerializedName("creator")
	val creator: Creator? = null,

	@SerializedName("is_favorited")
	val isFavorited: Boolean = false,

	@SerializedName("group_details")
	val groupDetails: GroupDetails? = null,

	@SerializedName("time_details")
	val timeDetails: TimeDetails? = null,

	@SerializedName("session_details")
	val sessionDetails: SessionDetails? = null,

	@SerializedName("creation_date")
	val creationDate: String? = null,

	@SerializedName("title")
	val title: String? = null,

	@SerializedName("creator_role")
	val creatorRole: Int? = null,

	@SerializedName("id")
	val id: String? = null,

	@SerializedName("is_online")
	val isOnline: Boolean? = null,

	@SerializedName("status")
	val status: Status? = null,

	@SerializedName("media_urls")
	val mediaUrls: List<String>? = null
)