package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class SessionDetails(

	@SerializedName("session_individual_count")
	val sessionIndividualCount: Int? = null,

	@SerializedName("session")
	val session: Session? = null,

	@SerializedName("session_series_count")
	val sessionSeriesCount: Int? = null
)