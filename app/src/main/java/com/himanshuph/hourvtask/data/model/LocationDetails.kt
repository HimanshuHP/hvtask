package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class LocationDetails(

	@SerializedName("location_address")
	val locationAddress: String? = null,

	@SerializedName("pincode")
	val pincode: String? = null,

	@SerializedName("location_name")
	val locationName: String? = null,

	@SerializedName("lng")
	val lng: String? = null,

	@SerializedName("location_id")
	val locationId: String? = null,

	@SerializedName("location_radius")
	val locationRadius: String? = null,

	@SerializedName("lat")
	val lat: String? = null
)