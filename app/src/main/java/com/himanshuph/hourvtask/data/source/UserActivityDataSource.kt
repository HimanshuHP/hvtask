package com.himanshuph.hourvtask.data.source

import com.himanshuph.hourvtask.data.model.ActivitiesItem
import com.himanshuph.hourvtask.data.model.Result
import io.reactivex.Observable

interface UserActivityDataSource {

    fun getUserActivities(userId:String,role:String,query:String="") : Observable<Result<ArrayList<ActivitiesItem>>>
}