package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class Status(

	@SerializedName("code")
	val code: Int? = null,

	@SerializedName("label")
	val label: String? = null
)