package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class Session(

	@SerializedName("date")
	val date: String? = null,

	@SerializedName("location_details")
	val locationDetails: LocationDetails? = null,

	@SerializedName("id")
	val id: String? = null
)