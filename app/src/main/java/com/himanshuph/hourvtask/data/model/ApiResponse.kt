package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class ApiResponse(

        @SerializedName("code")
        val code: Int = 0,

        @SerializedName("activities_count")
        val activitiesCount: Int = 0,

        @SerializedName("activities")
        val activities: ArrayList<ActivitiesItem>? = null,

        @SerializedName("message")
        val message: String = "",

        @SerializedName("error_details")
        val errorMsg: String? = null
)