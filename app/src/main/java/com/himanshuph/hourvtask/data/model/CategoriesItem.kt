package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class CategoriesItem(

	@SerializedName("name")
	val name: String? = null,

	@SerializedName("id")
	val id: String? = null
)