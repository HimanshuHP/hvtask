package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class TimeDetails(

	@SerializedName("time_needed")
	val timeNeeded: Int? = null,

	@SerializedName("label")
	val label: String? = null
)