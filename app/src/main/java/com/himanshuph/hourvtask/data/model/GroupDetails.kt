package com.himanshuph.hourvtask.data.model


import com.google.gson.annotations.SerializedName


data class GroupDetails(

	@SerializedName("time_needed_per_participant")
	val timeNeededPerParticipant: Int? = null,

	@SerializedName("num_participants")
	val numParticipants: Int? = null
)