package com.himanshuph.hourvtask.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

object CommonUtils {

    fun getFormattedDate(dateInString: String): String {
        var formattedDate = dateInString
        val sdf1 = SimpleDateFormat("yyyy-MM-dd")

        val sdf2 = SimpleDateFormat("dd MMM")
        try {

            val date: Date = sdf1.parse(dateInString)
            formattedDate = sdf2.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return formattedDate
    }
}